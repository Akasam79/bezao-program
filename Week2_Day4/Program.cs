﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Week2_Day4
{
    public delegate void Notify();
    class Program
    {
        static void Main(string[] args)
        {
            //Action Delegate. Unlike FUNC delegate cannot take a void
            //return type amd the Action delegate acts as part of the class.
            Action<int> printActionDel = ConsolePrint;
            printActionDel(10);

            //Predicate<string> isUpper = isUpperCase;
            //bool result = isUpper("hello world");
            //Console.WriteLine(result);

            Predicate<string> isUpper = s => s.Equals(s.ToUpper());
            bool result = isUpper("HELLO WORLD!!");
            Console.WriteLine(result);

            var items = new string[] { "pen", "book", "eraser" };

            var containsUpperCaseItems = items.Any(item => item.Equals(item.ToUpper()));

            Console.WriteLine(containsUpperCaseItems);

           
            ProcessBusinessLogic BL = new ProcessBusinessLogic();
            BL.ProcessCompleted += bl_ProcessCompleted; // register with an event
            //BL.ProcessCompleted += theNextProcess;
            //BL.ProcessCompleted += anotherProcess;
            BL.StartProcess();
            
            
        }

        public static void bl_ProcessCompleted(object sender, bool IsSuccessful)
        {
            Console.WriteLine("Process" + (IsSuccessful? "Completed Successfully": "failed"));
        }
        /*public static void anotherProcess()
        {
             Console.WriteLine("the 2nd process completed");
        }
        public static void theNextProcess()
        {
            Console.WriteLine("the 3rd process");
        }*/
        static void ConsolePrint(int i)
        {
            Console.WriteLine(i);
        }

        



        static bool isUpperCase(string str)
        {
            return str.Equals(str.ToUpper());
        }

    }
    public class ProcessBusinessLogic
    {
        // access modifier(Public/Private) + keyword event + delegate Name(Notify) + event Name
        public event EventHandler<bool> ProcessCompleted;
        public void StartProcess()
        {
            try
            {
                Console.WriteLine("Process Started");
                OnProcessCompleted(true);
            }
            catch(Exception ex)
            {
                OnProcessCompleted(false);
            }
            
        }

        protected virtual void OnProcessCompleted(bool IsSuccessful)
        {
            ProcessCompleted?.Invoke(this, IsSuccessful);// this means that if process completed is not null, then Invoke. we call also use
                                       // .Equals(null) with if statement
        }
    }


}
