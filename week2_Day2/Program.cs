﻿using System;

namespace week2_Day2
{

    public delegate void CustomDelegate(string msg);
    public delegate int CustomDelegate2();

    public delegate T add<T>(T param1, T param2);
    class Program
    {
        static void Main(string[] args)
        {
            //int a = 5;
            //int b = 10;
            //Console.WriteLine(a.IsGreaterThan(b));

            //Console.WriteLine(a.SquareOfNum());

            //CustomDelegate del = new CustomDelegate(MethodA);
            //del("Bezao");

            //CustomDelegate del = Logger.Log;
            //del( "samuel");

            //CustomDelegator.customizer("");

            //CustomDelegate2 engineDelegate = Engine.GetOilLevel;
            //CustomDelegate2 notificationDelegate2 = Break.GetHydraulicPressure;
            //CustomDelegate2 del = engineDelegate + notificationDelegate2;

            //Console.WriteLine(del());

            //add<int> sum = Sum;
            //Console.WriteLine(sum(0, 1));

            //add<string> con = Concat;
            //Console.WriteLine(Concat("Hello", " Bezao"));

            Func<int> getRandomNumber = delegate ()
            {
                Random rand = new Random();
                return rand.Next(1, 100);
            };

            Console.WriteLine(getRandomNumber());

            Func<int, int, int> add = Sum;
            var result = add(10, 10);

            Console.WriteLine(result);

            
         }
       

        public static int Sum (int x, int y)
        {
            return x + y;
        }
        //public static int Sum (int num1, int num2)
        //{
        //    return num1 + num2;
        //}

        public static string Concat (string str1, string str2)
        {
            return str1 + str2;
        }
           
            
    }


    

  

           
        //static void MethodA(string message)
        //{
        //    Console.WriteLine(message);
        //}

    

    public class Engine
    {
        public static int GetOilLevel()
        {
            return 5;
        }
    }

    public class Break
    {
        public static int GetHydraulicPressure()
        {
            return 200;
        }
    }
    
    public class Logger
    {
        public static void Log(string message)
        {
            Console.WriteLine("we will call Logger method with parameter" + message);
        }
    }

    public class Notification
    {
        public static void Alert(string message)
        {
            Console.WriteLine("we will call Notification method with parameter" + message);
        }
    }

    public class CustomDelegator
    {
        public static void customizer (string message)
        {
            CustomDelegate logDelegate = Logger.Log;
            CustomDelegate notificationDelegate = Notification.Alert;
            CustomDelegate del = logDelegate + notificationDelegate;
            Console.WriteLine("After logDelegate + notificationDelegate");
            del("Hello World");

            CustomDelegate lambdaDelegate = (string msg) => Console.WriteLine("Called Lambda expression:" + msg);
            del += del - notificationDelegate;
            Console.WriteLine("After logDelegate + notificationDelegate + lambdaDelegate");
            del("Hello World");

            del = del - notificationDelegate;
            Console.WriteLine("After del - notificationDelegate");
            del("Hello world");

            del -= logDelegate;
            Console.WriteLine("After del1 - loggerDelegate");
            del("Hello World");
        }
    }


    //public static class CustomException
    //{
    //    public static bool IsGreaterThan(this int i, int value)
    //    {
    //        return i > value;
    //    }

    //    public static int SquareOfNum(this int a)
    //    {
    //        return a * a;
    //    }
    //}

}
