﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using week2_Day2;

namespace firstProject
{
    class Program
    {
        static void Main(string[] args)
        {

            //var numbers = Tuple.Create(1, 2, 3, 4, 5, 6, 7, Tuple.Create(8, 9, 10, 11, 12, 13, 14, Tuple.Create(15)));

            //Console.WriteLine(numbers.Item1);
            //Console.WriteLine(numbers.Item7);
            //Console.WriteLine(numbers.Rest.Item1);
            //Console.WriteLine(numbers.Rest.Item1.Item1);
            //Console.WriteLine(numbers.Rest.Item1.Item2);
            //Console.WriteLine(numbers.Rest.Item1.Rest);
            //    var user = Tuple.Create(1, "Mary", "Doe");
            //    Display(user);

            //}

            //static void Display (Tuple<int, string, string> user)
            //{
            //    Console.WriteLine($"Id = {user.Item1}");
            //    Console.WriteLine($"FirstName = {user.Item2}");
            //    Console.WriteLine($"LastName = {user.Item2}");
            //}

            //Method 1

            //ValueTuple<int, string, string> user = (1, "Mary", "Doe");
            //Console.WriteLine(user.Item1);
            //Console.WriteLine(user.Item2);
            //Console.WriteLine(user.Item3); 

            //(int, string, string) user = (1, "Mary", "Doe");
            //Console.WriteLine(user.Item1);
            //Console.WriteLine(user.Item2);
            //Console.WriteLine(user.Item3);

            //var number = (1); //for an item to be a tuple, it must have at least 2 tuples i.e this is an int not tuple
            //var numbers = (1, 2);
            //var Numbers = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11); // Value Tuple can now take more than 8 characters and no need for extension or Rest.

            //var user = (Id: 1, FirstName: "Mary", LastName: "Doe");
            //Console.WriteLine(user.Id);

            //Display((1, "Mary", "Doe"));

            //(int UserId, String FName, String LName) = GetUser();
            //Console.WriteLine(UserId);
            //Console.WriteLine(FName);
            //Console.WriteLine(LName);

            //(var UserId, var FName, var LName) = GetUser();

            //(var id, var FName2, _) = GetUser();

            //Console.WriteLine(UserId);
            //Console.WriteLine(FName);
            //Console.WriteLine(LName);

            //List<int> list = new List<int>() { 1, 2, 3, 4, 5, 6 };
            //List<int> oddNumbers = list.FindAll(x => (x % 2) != 0);

            //foreach(var num in oddNumbers)
            //{
            //    Console.WriteLine("{0}", num);
            //}
            //Console.WriteLine();
            //Console.WriteLine(sum(1, 3));

            //try
            //{
            //    Console.WriteLine("Enter a number:");
            //    var num = int.Parse(Console.ReadLine());
            //    Console.WriteLine($"Square of {num} is {num * num}");

            //}
            //catch(Exception ex)
            //{
            //    Console.WriteLine("Error occurred:" + ex.Message);
            //}
            //finally
            //{
            //    Console.Write("Re-try with a different number");
            //}

            //Console.Write("Please enter a number to divide 100 :");

            //try
            //{
            //    double num = int.Parse(Console.ReadLine());
            //    double result = 100 / num;
            //    Console.WriteLine("100/ {0} = {1}", num, result);

            //}
            //catch (DivideByZeroException ex)
            //{
            //    Console.Write("Cannot divide by Zero, Please try again: " + ex.Message);
            //}
            //catch (InvalidOperationException ex)
            //{
            //    Console.Write("Invalid operation. Please try again");
            //}
            //catch (FormatException ex)
            //{
            //    Console.Write("Not a Valid format. Please try again");
            //}
            //catch (Exception ex)
            //{
            //    Console.Write("Error occurred. Please try again");
            //}
            //finally
            //{

            //}

            //try
            //{
            //    Method1();
            //}
            //catch(Exception ex)
            //{
            //    Console.WriteLine(ex.StackTrace);
            //}

            //Student newStudent = null;
            //try
            //{
            //    newStudent = new Student();
            //    newStudent.StudentName = "James007";

            //    ValidateStudent(newStudent);
            //}
            //catch(InvalidStudentNameException ex)
            //{
            //    Console.WriteLine(ex.Message);
            //}

            //Console.ReadKey();
        
           
        }

       

        //private static void ValidateStudent(Student std)
        //{
        //    Regex regex = new Regex("^[a-zA-Z]+$");

        //    if (!regex.IsMatch(std.StudentName))
        //        throw new InvalidStudentNameException(std.StudentName);
        //}


        //static void Method1()
        //{
        //    try
        //    {
        //        Method2();
        //    }
        //    catch(Exception ex) {
        //        throw;
        //    }
        //}
        //static void Method2()
        //{
        //    string str = null;
        //    try
        //    {
        //        Console.WriteLine(str[0]);
        //    }
        //    catch(Exception ex)
        //    {
        //        throw;
        //    }
        //}

        static int sum(int a, int b)
        {
           return  a + b;
        }

        
        //static void Display ((int, string, string ) user)
        //{
        //    Console.WriteLine($"Id = {user.Item1}");
        //    Console.WriteLine($"First Name = {user.Item2}");
        //    Console.WriteLine($"Last Name = {user.Item3}");
        //}

        //static (int, string, string) GetUser()
        //{
        //    return (Id: 1, FirstName: "Mary", LastName: "Doe");

        //}
        


    }
    
}
